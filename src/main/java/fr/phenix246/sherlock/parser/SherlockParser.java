package fr.phenix246.sherlock.parser;

import fr.phenix246.sherlock.parser.ast.ComparisonOperator;
import fr.phenix246.sherlock.parser.ast.Node;
import fr.phenix246.sherlock.parser.ast.NodesFactory;
import fr.phenix246.sherlock.parser.ast.SherlockOperators;
import fr.phenix246.sherlock.parser.exception.SherlockParserException;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Set;

/**
 * Parser of the SherlockQL.
 *
 * <p>SherlockQL is a query language for parametrized filtering of entries in RESTful APIs. It's a
 * superset of the <a href="http://tools.ietf.org/html/draft-nottingham-atompub-fiql-00">FIQL</a>
 * (Feed Item Query Language), so it can be used for parsing FIQL as well.</p>
 *
 * <p><b>Grammar in EBNF notation:</b>
 * <pre>{@code
 * input          = or, EOF;
 * or             = and, { ( "," | " or " ) , and };
 * and            = constraint, { ( ";" | " and " ), constraint };
 * constraint     = ( group | comparison );
 * group          = "(", or, ")";
 *
 * comparison     = selectors, comparator, arguments;
 * selectors      = ( "[", unreserved-str, { ( ",", ";" ) , unreserved-str }, "]" ) | unreserved-str;
 *
 * comparator     = comp-fiql | comp-alt;
 * comp-fiql      = ( ( "=", { ALPHA } ) | "!" ), "=";
 * comp-alt       = ( ( ">" | "<" ), [ "=" ] ) | ( ( "!" | "=" ) ( "~" | ":" ) );
 *
 * arguments      = ( "(", value, { "," , value }, ")" ) | value;
 * value          = unreserved-str | double-quoted | single-quoted;
 *
 * unreserved-str = unreserved, { unreserved }
 * single-quoted  = "'", { ( escaped | all-chars - ( "'" | "\" ) ) }, "'";
 * double-quoted  = '"', { ( escaped | all-chars - ( '"' | "\" ) ) }, '"';
 *
 * reserved       = '"' | "'" | "(" | ")" | ";" | "," | "=" | "!" | "~" | "<" | ">" | ":" |  " ";
 * unreserved     = all-chars - reserved;
 * escaped        = "\", all-chars;
 * all-chars      = ? all unicode characters ?;
 * }</pre>
 *
 * @version 1.0
 */
public class SherlockParser {

    private static final Charset ENCODING = StandardCharsets.UTF_8;

    private final NodesFactory nodesFactory;


    /**
     * Creates a new instance of {@code RSQLParser} with the default set of comparison operators.
     */
    public SherlockParser() {
        this.nodesFactory = new NodesFactory(SherlockOperators.defaultOperators());
    }

    /**
     * Creates a new instance of {@code RSQLParser} that supports only the specified comparison
     * operators.
     *
     * @param operators A set of supported comparison operators. Must not be <tt>null</tt> or empty.
     */
    public SherlockParser(Set<ComparisonOperator> operators) {
        if (operators == null || operators.isEmpty()) {
            throw new IllegalArgumentException("operators must not be null or empty");
        }
        this.nodesFactory = new NodesFactory(operators);
    }

    /**
     * Parses the RSQL expression and returns AST.
     *
     * @param query The query expression to parse.
     * @return A root of the parsed AST.
     *
     * @throws SherlockParserException If some exception occurred during parsing, i.e. the
     *          {@code query} is syntactically invalid.
     * @throws IllegalArgumentException If the {@code query} is <tt>null</tt>.
     */
    public Node parse(String query) throws SherlockParserException {
        if (query == null) {
            throw new IllegalArgumentException("query must not be null");
        }
        InputStream is = new ByteArrayInputStream(query.getBytes(ENCODING));
        Parser parser = new Parser(is, ENCODING.name(), nodesFactory);

        try {
            return parser.Input();

        } catch (Exception | TokenMgrError ex) {
            throw new SherlockParserException(ex);
        }
    }

}
