package fr.phenix246.sherlock.parser.ast;

public enum LogicalOperator {

    AND (";"),
    OR  (",");

    private final String symbol;

    LogicalOperator(String symbol) {
        this.symbol = symbol;
    }

    public static LogicalOperator fromToken(String s) {
        for (LogicalOperator value : values()) {
            if(value.symbol.equals(s))
                return value;
        }
        return null;
    }

    @Override
    public String toString() {
        return symbol;
    }
}
