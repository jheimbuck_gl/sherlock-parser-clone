package fr.phenix246.sherlock.parser.ast;

import java.util.ArrayList;
import java.util.List;

import static fr.phenix246.sherlock.parser.ast.StringUtils.join;

/**
 * This node represents a comparison with operator, selector and arguments,
 * e.g. <tt>name=in=(Jimmy,James)</tt>.
 */
public final class ComparisonNode extends AbstractNode {

    private final ComparisonOperator operator;

    private final List<String> selectors;

    private final List<String> arguments;

    private final LogicalOperator  selectorsOperator;
    /**
     * @param operator Must not be <tt>null</tt>.
     * @param selector Must not be <tt>null</tt> or blank.
     * @param arguments Must not be <tt>null</tt> or empty. If the operator is not
     *          {@link ComparisonOperator#isMultiValue() multiValue}, then it must contain exactly
     *          one argument.
     *
     * @throws IllegalArgumentException If one of the conditions specified above it not met.
     */
    public ComparisonNode(ComparisonOperator operator, String selector, List<String> arguments) {
        Assert.notNull(operator, "operator must not be null");
        Assert.notBlank(selector, "selector must not be blank");
        Assert.notEmpty(arguments, "arguments list must not be empty");
        Assert.isTrue(operator.isMultiValue() || arguments.size() == 1,
                "operator %s expects single argument, but multiple values given", operator);

        this.operator = operator;
        this.selectors = new ArrayList<>();
        this.selectors.add(selector);
        this.arguments = new ArrayList<>(arguments);
        this.selectorsOperator = LogicalOperator.OR;
    }

    public ComparisonNode(ComparisonOperator operator, List<String> selectors, List<String> arguments) {
        Assert.notNull(operator, "operator must not be null");
        Assert.notEmpty(arguments, "arguments list must not be empty");
        Assert.notEmpty(selectors, "selectors list must not be empty");
        Assert.isTrue(operator.isMultiValue() || arguments.size() == 1,
                "operator %s expects single argument, but multiple values given", operator);
        this.operator = operator;
        if(selectors.size() > 2) {
            this.selectorsOperator = LogicalOperator.fromToken(selectors.get(0));
            selectors.remove(0);
        } else {
            this.selectorsOperator = LogicalOperator.OR;
        }
        this.selectors = new ArrayList<>(selectors);
        this.arguments = new ArrayList<>(arguments);
    }

    public ComparisonNode(ComparisonOperator operator, LogicalOperator selectorsOperator, List<String> selectors, List<String> arguments) {
        Assert.notNull(operator, "operator must not be null");
        Assert.notNull(selectorsOperator, "selectorsOperator must not be null");
        Assert.notEmpty(arguments, "arguments list must not be empty");
        Assert.notEmpty(selectors, "selectors list must not be empty");
        Assert.isTrue(operator.isMultiValue() || arguments.size() == 1,
                "operator %s expects single argument, but multiple values given", operator);
        this.operator = operator;
        this.selectorsOperator = selectorsOperator;
        this.selectors = new ArrayList<>(selectors);
        this.arguments = new ArrayList<>(arguments);
    }


    public <R, A> R accept(SherlockVisitor<R, A> visitor, A param) {
        return visitor.visit(this, param);
    }

    public ComparisonOperator getOperator() {
        return operator;
    }

    /**
     * Returns a copy of this node with the specified operator.
     *
     * @param newOperator Must not be <tt>null</tt>.
     */
    public ComparisonNode withOperator(ComparisonOperator newOperator) {
        return new ComparisonNode(newOperator, selectors, arguments);
    }

    /**
     * Returns a copy of the selectors list. It's guaranteed that it contains at least one item.
     */
    public List<String> getSelectors() {
        return new ArrayList<>(selectors);
    }

    public LogicalOperator getSelectorsOperator() {
        return selectorsOperator;
    }

    /**
     * Returns a copy of this node with the specified selector.
     *
     * @param newSelector Must not be <tt>null</tt> or blank.
     */
    public ComparisonNode withSelector(String newSelector) {
        return new ComparisonNode(operator, newSelector, arguments);
    }

    /**
     * Returns a copy of this node with the specified selector.
     *
     * @param newSelectors Must not be <tt>null</tt> or empty.
     */
    public ComparisonNode withSelectors(List<String> newSelectors) {
        return new ComparisonNode(operator, newSelectors, arguments);
    }

    /**
     * Returns a copy of the arguments list. It's guaranteed that it contains at least one item.
     * When the operator is not {@link ComparisonOperator#isMultiValue() multiValue}, then it
     * contains exactly one argument.
     */
    public List<String> getArguments() {
        return new ArrayList<>(arguments);
    }

    /**
     * Returns a copy of this node with the specified arguments.
     *
     * @param newArguments Must not be <tt>null</tt> or empty. If the operator is not
     *          {@link ComparisonOperator#isMultiValue() multiValue}, then it must contain exactly
     *          one argument.
     */
    public ComparisonNode withArguments(List<String> newArguments) {
        return new ComparisonNode(operator, selectors, newArguments);
    }


    @Override
    public String toString() {
        String args = arguments.size() > 1
                ? "('" + join(arguments, "','") + "')"
                : "'" + arguments.get(0) + "'";
        String selectors = this.selectors.size() > 1
                ? "[" + join(this.selectors, this.selectorsOperator.toString()) + "]"
                : this.selectors.get(0) ;
        return selectors + operator + args;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ComparisonNode)) return false;
        ComparisonNode that = (ComparisonNode) o;

        return arguments.equals(that.arguments)
                && operator.equals(that.operator)
                && selectors.equals(that.selectors)
                && selectorsOperator.equals(that.selectorsOperator);
    }

    @Override
    public int hashCode() {
        int result = selectors.hashCode();
        result = 31 * result + arguments.hashCode();
        result = 31 * result + operator.hashCode();
        result = 31 * result + selectorsOperator.hashCode();
        return result;
    }
}
