package fr.phenix246.sherlock.parser.ast;

/**
 * An adapter for the {@link SherlockVisitor} interface with a simpler contract that omits the optional
 * second argument.
 *
 * @param <R> Return type of the visitor's method.
 */
public abstract class NoArgSherlockVisitorAdapter<R> implements SherlockVisitor<R, Void> {

    public abstract R visit(AndNode node);

    public abstract R visit(OrNode node);

    public abstract R visit(ComparisonNode node);


    public R visit(AndNode node, Void param) {
        return visit(node);
    }

    public R visit(OrNode node, Void param) {
        return visit(node);
    }

    public R visit(ComparisonNode node, Void param) {
        return visit(node);
    }
}
