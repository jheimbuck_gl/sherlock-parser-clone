package fr.phenix246.sherlock.parser.ast;

import java.util.HashSet;
import java.util.Set;

import static java.util.Arrays.asList;

public class SherlockOperators {
    public static final ComparisonOperator
            EQUAL = new ComparisonOperator("=="),
            NOT_EQUAL = new ComparisonOperator("!="),
            GREATER_THAN = new ComparisonOperator("=gt=", ">"),
            GREATER_THAN_OR_EQUAL = new ComparisonOperator("=ge=", ">="),
            LESS_THAN = new ComparisonOperator("=lt=", "<"),
            LESS_THAN_OR_EQUAL = new ComparisonOperator("=le=", "<="),
            LIKE = new ComparisonOperator("=like=", "=~"),
            NOT_LIKE = new ComparisonOperator("=nlike=", "!~"),
            IN = new ComparisonOperator("=in=", "=:",true),
            NOT_IN = new ComparisonOperator("=out=", "!:", true);


    public static Set<ComparisonOperator> defaultOperators() {
        return new HashSet<>(asList(EQUAL, NOT_EQUAL, GREATER_THAN, GREATER_THAN_OR_EQUAL,
                LESS_THAN, LESS_THAN_OR_EQUAL, LIKE, IN, NOT_LIKE, NOT_IN));
    }
}
