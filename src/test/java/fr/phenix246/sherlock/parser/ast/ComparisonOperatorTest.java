package fr.phenix246.sherlock.parser.ast;

import org.junit.Test;
import static org.junit.Assert.*;

public class ComparisonOperatorTest {

    @Test
    public void testValidOperators() {
        var symbols = new String[]{"=foo=", "==", "!=", "<", ">", "<=", ">=", "=:", "!:", "=~", "!~"};
        for (String symbol : symbols) {
            new ComparisonOperator(symbol);
            assertTrue(symbol, true);
        }
    }

    @Test
    public void testInvalidOperators() {
        var symbols = new String[]{null, "", "foo", "=123=", "=", "=<", "=>", "=!", "a=b=c"};
        for (String symbol : symbols) {
            try {
                new ComparisonOperator(symbol);
                fail(symbol);
            } catch (IllegalArgumentException e) {
                assertTrue(true);
            }

        }
    }

    @Test
    public void TestEqual() {
        var op1 = new ComparisonOperator("=out=", "=notin=");
        var op2 = new ComparisonOperator("=out=", "=notin=", true);
        assertEquals(op1, op2);
    }
}
