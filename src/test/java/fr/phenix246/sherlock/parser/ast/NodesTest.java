package fr.phenix246.sherlock.parser.ast;

import fr.phenix246.sherlock.parser.SherlockParser;
import org.junit.Test;
import static org.junit.Assert.*;

public class NodesTest {

    @Test
    public void TestToString() {
        var input = new String[]{
                "genres=in=(sci-fi,action)",
                "name==\"Kill Bill\";year=gt=2003",
                "a<=1;b!=2;c>3",
                "a=gt=1,b==2;c!=3,d=lt=4"
        };

        var output = new String[]{
                "genres=in=('sci-fi','action')",
                "(name=='Kill Bill';year=gt='2003')",
                "(a=le='1';b!='2';c=gt='3')",
                "(a=gt='1',(b=='2';c!='3'),d=lt='4')"
        };

        for(int i = 0; i < input.length; i++) {
            var rootNode = new SherlockParser().parse(input[i]);
            assertEquals(output[i], rootNode.toString());
        }
    }
}
