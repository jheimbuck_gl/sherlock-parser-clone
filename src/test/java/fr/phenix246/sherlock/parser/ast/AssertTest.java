package fr.phenix246.sherlock.parser.ast;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.*;

public class AssertTest {

    @Test(expected = IllegalArgumentException.class)
    public void testIsTrue1() {
        Assert.isTrue(false, "msg");
    }

    @Test
    public void testIsTrue2() {
        Assert.isTrue(true, "msg");
        assertTrue(true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNotNull1() {
        Assert.notNull(null, "msg");
    }

    @Test
    public void testNotNull2() {
        Assert.notNull(new Object(), "msg");
        assertTrue(true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNotEmpty1() {
        Assert.notEmpty((Collection<?>)null, "msg");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNotEmpty2() {
        Assert.notEmpty(new Object[0], "msg");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNotEmpty3() {
        Assert.notEmpty(new ArrayList<>(), "msg");
    }

    @Test
    public void testNotEmpty4() {
        Assert.notEmpty(new Object[1], "msg");
        assertTrue(true);
    }

    @Test
    public void testNotEmpty5() {
        Collection<Integer> l = new ArrayList<>();
        l.add(1);
        Assert.notEmpty(l, "msg");
        assertTrue(true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNotBlank1() {
        Assert.notBlank(null, "msg");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNotBlank2() {
        Assert.notBlank("  ", "msg");
    }

    @Test
    public void testNotBlank3() {
        Assert.notBlank("a ", "msg");
        assertTrue(true);
    }
}
