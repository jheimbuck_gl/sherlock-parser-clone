package fr.phenix246.sherlock.parser;

import fr.phenix246.sherlock.parser.ast.*;
import fr.phenix246.sherlock.parser.exception.SherlockParserException;
import fr.phenix246.sherlock.parser.exception.UnknownOperatorException;
import org.junit.Test;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static fr.phenix246.sherlock.parser.ast.LogicalOperator.*;
import static fr.phenix246.sherlock.parser.ast.SherlockOperators.*;
import static org.junit.Assert.*;

public class SherlockParserTest {

    static final char[] RESERVED = new char[]{'\'', '(', ')', ';', ',', '=', '<', '>', '!', '~'};

    private final NodesFactory FACTORY = new NodesFactory(defaultOperators());

    @Test(expected = IllegalArgumentException.class)
    public void testCreateNull() {
        new SherlockParser(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateEmpty() {
        new SherlockParser(Set.of());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseNull() {
        parse(null);
    }

    @Test
    public void testParseOperator() throws UnknownOperatorException {
        for (ComparisonOperator operator : defaultOperators()) {
            var op = operator.getSymbol().trim();
            var expected = FACTORY.createComparisonNode(op, "sel", List.of("val"));
            assertEquals(expected,parse("sel"+op+"val"));
        }
    }

    @Test(expected = SherlockParserException.class)
    public void testParseShortOperator() {
        parse("sel=val");
    }

    @Test
    public void testParseSelector() {
        var input = new String[]{"allons-y", "l00k.dot.path", "look/XML/path", "n:look/n:xml", "path.to::Ref", "$doll_r.way"};
        for (var in: input) {
            var actual = parse(in+"==val");
            var expected = eq(in,"val");
            assertEquals(expected, actual);
        }
    }

    @Test
    public void testReservedSelector() {

        var input = stream(RESERVED).map(r-> "ill"+r).collect(Collectors.toList());
        for (var in: input) {
            try {
                parse(in+"==val");
                fail();
            } catch (SherlockParserException spe) {
                assertTrue(true);
            }
        }
    }

    @Test(expected = SherlockParserException.class)
    public void testEmptySelector() {
        parse("==val");
    }

    @Test
    public void testParseUnquotedArgument() {
        var input = new String[]{"«Allons-y»", "h@llo", "*star*", "ces*ký", "42", "0.15", "3:15"};
        for (var in: input) {
            var expected = eq("sel", in);
            var actual = (ComparisonNode)parse("sel=="+in);
            assertEquals(expected, actual);
        }
    }

    @Test
    public void testReservedArguments() {
        var input = stream(RESERVED).map(r-> "ill"+r).collect(Collectors.toList());
        for (var in: input) {
            try {
                parse("sel=="+in);
                fail();
            } catch (SherlockParserException spe) {
                assertTrue(true);
            }
        }
    }

    @Test
    public void testParseAnyChars() {
        var input = new String[]{"\"hi there!\"", "\"Pěkný den!\"", "\"Flynn's *\"", "\"o)'O'(o\"", "\"6*7=42\""};
        for (var in: input) {
            var expected = eq("sel", in.substring(1, in.length()-1));
            var actual = parse("sel=="+in);
            assertEquals(expected, actual);
        }
    }

    @Test
    public void testParseEscapedSingleArgument() {
        var input = new String[]{"'10\\' 15\"'", "'10\\' 15\\\"'", "'w\\\\ \\'Flyn\\n\\''", "'\\\\(^_^)/'"};
        var output = new String[]{"10' 15\"", "10' 15\"", "w\\ 'Flynn'", "\\(^_^)/"};
        for (int i = 0; i < input.length; i++) {
            var expected = eq("sel", output[i]);
            var actual = parse("sel=="+ input[i]);
            assertEquals(expected, actual);
        }
    }

    @Test
    public void testParseEscapedDoubleArgument() {
        var input = new String[]{"\"10' 15\""};
        var output = new String[]{"10' 15"};
        for (int i = 0; i < input.length; i++) {
            var expected = eq("sel", output[i]);
            var actual = parse("sel=="+ input[i]);
            assertEquals(expected, actual);
        }
    }

    @Test
    public void testParseArgumentsGroup() {
        var input = List.of(
                List.of("chunky", "bacon", "\"ftw!\""),
                List.of("'hi!'", "\"how're you?\""),
                List.of("meh"),
                List.of("\")o(\"")
        );
        for (var in: input) {
            var values = in.stream().map(s -> {
                if(s.startsWith("'") || s.startsWith("\"")) {
                    return s.substring(1, s.length()-1);
                } else {
                    return s;
                }
            }).collect(Collectors.toList());
            var expected = new ComparisonNode(IN, "sel", values);
            var actual = parse("sel=in=("+String.join(",", in)+")");
            assertEquals(expected, actual);
        }
    }

    @Test
    public void testParseSelectorsGroup() {
        var input = List.of(
                List.of("Hello", "darkness", "my"),
                List.of("old", "friend"),
                List.of("its", "me")
        );

        // Test Or
        for (var in: input) {
            var values = in.stream().map(s -> {
                if(s.startsWith("'") || s.startsWith("\"")) {
                    return s.substring(1, s.length()-1);
                } else {
                    return s;
                }
            }).collect(Collectors.toList());
            var expected = new ComparisonNode(LIKE, OR, values, List.of("value"));
            var actual = parse("["+String.join(",", in)+"]=~value");
            assertEquals(expected, actual);
        }

        // Test And
        for (var in: input) {
            var values = in.stream().map(s -> {
                if(s.startsWith("'") || s.startsWith("\"")) {
                    return s.substring(1, s.length()-1);
                } else {
                    return s;
                }
            }).collect(Collectors.toList());
            var expected = new ComparisonNode(LIKE, AND, values, List.of("value"));
            var actual = parse("["+String.join(";", in)+"]=~value");
            assertEquals(expected, actual);
        }

        // Test One
        var values = List.of("again");
        var expected = new ComparisonNode(LIKE, OR, values, List.of("value"));
        var actual = parse("["+String.join(";", values)+"]=~value");
        assertEquals(expected, actual);
    }

    @Test
    public void testParseLogicalOperator() {
        var ops = LogicalOperator.values();
        for (var op: ops) {
            var expected = FACTORY.createLogicalNode(op, List.of(eq("sel1", "arg1"), eq("sel2", "arg2")));
            var actual = parse("sel1==arg1"+op.toString()+"sel2==arg2");
            assertEquals(expected, actual);
        }
    }

    @Test
    public void testParseAlternativeLogicalOperator() {
        var ops = LogicalOperator.values();
        for (var op: ops) {
            var expected = FACTORY.createLogicalNode(op, List.of(eq("sel1", "arg1"), eq("sel2", "arg2")));
            var alt = op == LogicalOperator.AND ? " and " : " or ";
            var actual = parse("sel1==arg1"+alt+"sel2==arg2");
            assertEquals(expected, actual);
        }
    }

    @Test
    public void testParseQueryWithDefaultOperatorPriority() {
        var input = new String[]{"s0==a0;s1==a1;s2==a2", "s0==a0,s1=out=(a10,a11),s2==a2", "s0==a0,s1==a1;s2==a2,s3==a3"};
        var output = new Node[]{
                and(eq("s0","a0"), eq("s1","a1"), eq("s2","a2")),
                or(eq("s0","a0"), out("s1","a10", "a11"), eq("s2","a2")),
                or(eq("s0","a0"), and(eq("s1","a1"), eq("s2","a2")), eq("s3","a3"))
        };

        for(int i = 0; i < input.length; i++) {
            var expected = output[i];
            var actual = parse(input[i]);
            assertEquals(expected, actual);
        }
    }

    @Test
    public void testParseQueryWithParenthesis() {
        var input = new String[]{
                "(s0==a0,s1==a1);s2==a2",
                "(s0==a0,s1=out=(a10,a11));s2==a2,s3==a3",
                "((s0==a0,s1==a1);s2==a2,s3==a3);s4==a4",
                "(s0==a0)",
                "((s0==a0));s1==a1"
        };
        var output = new Node[]{
                and(or(eq("s0","a0"), eq("s1","a1")), eq("s2","a2")),
                or(and(or(eq("s0","a0"), out("s1","a10", "a11")), eq("s2","a2")), eq("s3","a3")),
                and(or(and(or(eq("s0","a0"), eq("s1","a1")), eq("s2","a2")), eq("s3","a3")), eq("s4","a4")),
                eq("s0", "a0"),
                and(eq("s0", "a0"), eq("s1","a1"))
        };


        for(int i = 0; i < input.length; i++) {
            var expected = output[i];
            var actual = parse(input[i]);
            assertEquals(expected, actual);
        }
    }

    @Test
    public void testThrowExceptionForUnclosedParenthesis() {
        var input = new String[]{"(s0==a0;s1!=a1", "s0==a0)", "s0==a;(s1=in=(b,c),s2!=d" };
        for (var in: input) {
            try {
                parse(in);
                fail();
            } catch (SherlockParserException spe) {
                assertTrue(true);
            }
        }
    }

    @Test
    public void testUseParserWithCustomOperators() {
        var allOperator = new ComparisonOperator("=all=", true);
        var parser = new SherlockParser(Set.of(EQUAL, allOperator));
        var expected = and(eq("name", "TRON"), new ComparisonNode(allOperator, "genres", List.of("sci-fi", "thriller")));

        var actual = parser.parse("name==TRON;genres=all=(sci-fi,thriller)");

        assertEquals(expected, actual);

        try {
            parser.parse("name==TRON;year=ge=2010");
            fail();
        } catch (SherlockParserException spe) {
            assertTrue(spe.getCause() instanceof UnknownOperatorException);
            var uoe = (UnknownOperatorException)spe.getCause();
            assertEquals("=ge=",uoe.getOperator());
        }

    }

    //////// Helpers ////////

    public Stream<Character> stream(char[] chars) { return IntStream.range(0, chars.length).mapToObj(i -> chars[i]); }
    public Node parse(String query) { return new SherlockParser().parse(query); }
    public AndNode and(Node... nodes) { return new AndNode(List.of(nodes)); }
    public OrNode or(Node... nodes) { return new OrNode(List.of(nodes)); }
    public ComparisonNode eq(String sel, String arg) { return new ComparisonNode(EQUAL, sel, List.of(arg)); }
    public ComparisonNode out(String sel, String... args) { return new ComparisonNode(NOT_IN, sel, List.of(args)); }
}
